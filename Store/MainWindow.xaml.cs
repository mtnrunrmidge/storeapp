﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Store
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Update inventory window 
        private UpdateInventory update;
        //Search window 
        private Search search;
        //Invoice number current
        private int invoiceNum = 0;

        /// <summary>
        /// Initialize the main window components
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// When the update inventory button is clicked it opens the Update Inventory window.
        /// Closes the main window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateInventory_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                update = new UpdateInventory();
                update.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Open a new search invoices window.
        /// Close the main window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchInvoices_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                search = new Search();
                search.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Exit the entire program when the exit button is selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Bring up a window without closing the main window to enter the customer information (?)
        /// Un-block the options to add items to the invoice. essentially just call the edit invoice functionality.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewInvoice_Click(object sender, RoutedEventArgs e)
        {
            //New invoice window to enter customer info (?)
            //populate the invoice number with automatic invoice number
            //call the edit invoice functionality
            EditInvoice();
        }

        /// <summary>
        /// Edit the invoice by adding, removing and saving items to the invoice.
        /// 
        /// </summary>
        private void EditInvoice()
        {
            //display the current invoice number
            invoiceNumberLbl.Content = invoiceNum.ToString();
            //populate the items drop down combo box with existing inventory items
            //Allow drop down options to add items to the invoice
            //allow option to remove items from the invoice.
            //refresh the data grid with invoice items in it
            //display current invoice total
            //allow option to delete invoice
            //allow option to save or cancel changes to the invoice.
        }

        /// <summary>
        /// Allow the Edit invoice method to be called if the button is edit. Change the button to save invoice.
        /// If save: then close the edit invoice functionality and change the button back to edit invoice.
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditISavenvoiceBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Delete the current invoice
        /// Only allow access to this button if there is an invoice displayed in this window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteInvoiceBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Cancel changes to the invoice.
        /// Only accessible if edit invoice was selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Add selected item to the current invoice.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Add_to_Invoice_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Remove selected item from the invoice if its in the invoice
        /// If not available in the invoice display a message.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeItemFromInvoice_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Display the invoice data in the data table.
        /// </summary>
        /// <param name="invoiceNum"></param>
        private void LoadInvoiceInfo(int invoiceNum)
        {

        }
    }
}
