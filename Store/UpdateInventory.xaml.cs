﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Store
{
    /// <summary>
    /// Interaction logic for UpdateInventory.xaml
    /// </summary>
    public partial class UpdateInventory : Window
    {
        //Main window
        private MainWindow main;

        /// <summary>
        /// Initialize the Update Inventory window comopnents
        /// </summary>
        public UpdateInventory()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Close the Update Window
        /// Open a main window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void close_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                main = new MainWindow();
                main.Show();
                this.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Delete the inventory item selected using the sql call to remove the item from the database
        /// Only allow if not in any invoices (?) or removes from invoices too (?)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void delete_btn_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Edits selected inventory item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void edit_btn_Click(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Add an item to the inventory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void add_btn_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
